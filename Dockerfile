FROM alpine:3.13

ARG PACKER_VERSION=1.7.2

RUN apk --no-cache add curl && \
    curl -o /packer.zip -L https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip && \
    unzip /packer.zip

# ---

FROM alpine:3.13

RUN apk --no-cache add tini vim curl ansible openssh-client && \
    mkdir /app
COPY --from=0 /packer /usr/local/bin
COPY /app /app
WORKDIR /app
ENTRYPOINT ["/sbin/tini", "--"]
CMD ["/usr/local/bin/packer", "build", "."]
