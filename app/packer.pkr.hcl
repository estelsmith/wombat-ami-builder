variable "digitalocean_token" {
  type = string
  default = ""
  sensitive = true
}

variable "digitalocean_size" {
  type = string
  default = "s-2vcpu-4gb"
}

variable "digitalocean_region" {
  type = string
  default = "nyc3"
}
variable "digitalocean_snapshot_regions" {
  type = list(string)
  default = ["nyc3"]
}

variable "skip_updates" {
  type = string
  default = ""
}

variable "ami_builder_name" {
  type = string
  default = "ami-builder"
}

variable "ami_builder_snapshot_name" {
  type = string
  default = "ami-builder-{{timestamp}}"
}

variable "ami_builder_snapshot_region" {
  type = string
  default = "nyc3"
}

source "digitalocean" "builder" {
  api_token = "${var.digitalocean_token}"
  communicator = "ssh"
  droplet_name = "${var.ami_builder_name}"
  region = "${var.digitalocean_region}"
  size = "${var.digitalocean_size}"
  image = "centos-8-x64"
  snapshot_name = "${var.ami_builder_snapshot_name}"
  snapshot_regions = ["${var.ami_builder_snapshot_region}"]
  ssh_username = "root"
}

build {
  name = "ami-builder-digitalocean"
  sources = [
    "source.digitalocean.builder"
  ]
  provisioner "ansible" {
    playbook_file = "./playbook.yml"
    user = "root"
    use_proxy = false
    extra_arguments = ["--extra-vars", "skip_updates=${var.skip_updates}"]
  }
}
